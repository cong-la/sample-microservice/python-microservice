from django.db import models

# Create your models here.
from django.shortcuts import render


class SchoolInfo(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(null=False, max_length=255)
    objects = models.Manager()

    class Meta:
        db_table = 'school_info'


