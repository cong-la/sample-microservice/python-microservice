from django.urls import path, include

from school_info.views import *

urlpatterns = [
    path('', get_all_school),
    path('<int:id>', get_school),
    path('add', add_school),
]