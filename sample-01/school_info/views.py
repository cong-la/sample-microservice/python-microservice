from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

from school_info.models import SchoolInfo
from school_info.school_info_serializer import SchoolInfoSerializer


@api_view(['GET'])
def get_all_school(request):
    all_schools = SchoolInfo.objects.all()
    all_schools_serializer = SchoolInfoSerializer(all_schools, many=True)
    response_data = all_schools_serializer.data
    return JsonResponse({'response': response_data}, safe=False, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_school(request, id):
    try:
        all_schools = SchoolInfo.objects.get(id=id)
        all_schools_serializer = SchoolInfoSerializer(all_schools, many=False)
        response_data = all_schools_serializer.data
        return JsonResponse({'response': response_data}, safe=False, status=status.HTTP_200_OK)
    except:
        return JsonResponse({'response': []}, safe=False,
                            status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
def add_school(request):
    school_data = JSONParser().parse(request)
    school_data_serializer = SchoolInfoSerializer(data=school_data)
    if school_data_serializer.is_valid():
        school_data_serializer.save()
        return JsonResponse({'response': school_data_serializer.data}, safe=False,
                            status=status.HTTP_201_CREATED)

    return JsonResponse({'response': "wrong input"}, safe=False,
                        status=status.HTTP_400_BAD_REQUEST)
