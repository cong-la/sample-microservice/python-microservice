from django.apps import AppConfig


class SchoolEventConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'school_event'
