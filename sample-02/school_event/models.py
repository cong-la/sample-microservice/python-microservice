from django.db import models

# Create your models here.
from django.db.models import Model


class SchoolEvent(Model):
    id = models.IntegerField(primary_key=True)
    school_id = models.IntegerField()
    name = models.CharField(null=False, max_length=255)
    objects = models.Manager()

    class Meta:
        db_table = 'school_event'
