from rest_framework.serializers import ModelSerializer

from school_event.models import SchoolEvent


class SchoolEventSerializer(ModelSerializer):
    class Meta:
        model = SchoolEvent
        fields = [
            'id',
            'school_id',
            'name'
        ]
